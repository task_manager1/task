package main

import (
	"embed"
	"gitlab.com/task_manager1/task/internal/app"
	"gitlab.com/task_manager1/task/internal/config"
	"gitlab.com/task_manager1/task/pkg/db/migrate"
)

//go:generate swag init

//go:embed migrations/*.sql
var embedMigrations embed.FS

// @title task
// @версия 1.0.0
// @description task
//
// @host task_manager-dev.kz
// @BasePath /task
// @schemes https http
func main() {
	cfg, err := config.GetConfig()
	if err != nil {
		panic(err)
	}

	migrate.SetBaseFS(embedMigrations)

	app.Run(cfg)
}
