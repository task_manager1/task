-- +goose Up
-- +goose StatementBegin
CREATE TABLE "task"."users" (
                        id BIGSERIAL PRIMARY KEY,
                        username VARCHAR(255),
                        password VARCHAR(255),
                        first_name VARCHAR(255),
                        last_name VARCHAR(255),
                        middle_name VARCHAR(255)
);

CREATE TABLE "task"."statuses" (
                          id SERIAL PRIMARY KEY,
                          name VARCHAR(255),
                          code INTEGER UNIQUE
);

CREATE TABLE "task"."tasks" (
                        id BIGSERIAL PRIMARY KEY,
                        title VARCHAR(255),
                        description VARCHAR(255),
                        creator BIGINT REFERENCES "task"."users"(id),
                        executor BIGINT REFERENCES "task"."users"(id),
                        status BIGINT REFERENCES "task"."statuses"(code),
                        created_at TIMESTAMP,
                        updated_at TIMESTAMP
);

CREATE TABLE "notifications"."notifications" (
                                id BIGSERIAL PRIMARY KEY,
                                title VARCHAR(255),
                                text VARCHAR(255),
                                task_id BIGINT REFERENCES "task"."tasks"(id),
                                user_id BIGINT REFERENCES "task"."users"(id),
                                is_read BOOLEAN
);

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- SELECT 'down SQL query';
-- +goose StatementEnd
