package v1

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/task_manager1/task/internal/common/middleware"
	"gitlab.com/task_manager1/task/internal/model"
	"gitlab.com/task_manager1/task/internal/schema"
	"strconv"
)

func (h *Handler) initTaskHandler(v1 *gin.RouterGroup) {
	v1.POST("/task", middleware.GinErrorHandle(h.CreateTask))
	v1.GET("/task", middleware.GinErrorHandle(h.GetTask))
	v1.GET("/tasks", middleware.GinErrorHandle(h.GetAllTasks))
	v1.DELETE("/task", middleware.GinErrorHandle(h.DeleteTask))
	v1.PUT("/task", middleware.GinErrorHandle(h.UpdateTask))
}

// CreateTask
// WhoAmi godoc
// @Summary Создание задачи
// @Accept json
// @Produce json
// @Param data body model.Task true "Task data"
// @Success 200 {object} schema.Response[schema.Empty]
// @Failure 400 {object} schema.Response[schema.Empty]
// @tags tasks
// @Router /api/v1/task [post]
func (h *Handler) CreateTask(c *gin.Context) error {
	ctx := c.Request.Context()
	var data model.Task

	if err := c.BindJSON(&data); err != nil {
		return err
	}

	err := h.services.Task.Create(ctx, data, data.Creator)
	if err != nil {
		return err
	}

	return schema.Respond("success", c)
}

// GetTask
// WhoAmi godoc
// @Summary Чтение задачи
// @Accept json
// @Produce json
// @Param id query uint64 true "Task ID"
// @Success 200 {object} schema.Response[model.Task]
// @Failure 400 {object} schema.Response[schema.Empty]
// @tags tasks
// @Router /api/v1/task [get]
func (h *Handler) GetTask(c *gin.Context) error {
	ctx := c.Request.Context()
	rawId := c.Query("id")
	id, err := strconv.ParseUint(rawId, 10, 64)
	if err != nil {
		return err
	}
	task, err := h.services.Task.Get(ctx, id)
	if err != nil {
		return err
	}
	return schema.Respond(task, c)
}

// DeleteTask
// WhoAmi godoc
// @Summary Удаление задачи
// @Accept json
// @Produce json
// @Param id query uint64 true "Task ID"
// @Success 200 {object} schema.Response[schema.Empty]
// @Failure 400 {object} schema.Response[schema.Empty]
// @tags tasks
// @Router /api/v1/task [delete]
func (h *Handler) DeleteTask(c *gin.Context) error {
	ctx := c.Request.Context()
	rawId := c.Query("id")
	id, err := strconv.ParseUint(rawId, 10, 64)
	if err != nil {
		return err
	}
	err = h.services.Task.Delete(ctx, id)
	if err != nil {
		return err
	}
	return schema.Respond("success", c)
}

// UpdateTask
// WhoAmi godoc
// @Summary Изменение задачи
// @Accept json
// @Produce json
// @Param data body model.Task true "Task data"
// @Success 200 {object} schema.Response[schema.Empty]
// @Failure 400 {object} schema.Response[schema.Empty]
// @tags tasks
// @Router /api/v1/task [put]
func (h *Handler) UpdateTask(c *gin.Context) error {
	var (
		request model.Task
		ctx     = c.Request.Context()
	)
	if err := c.ShouldBind(&request); err != nil {
		return err
	}

	err := h.services.Task.Update(ctx, request)
	if err != nil {
		return err
	}

	return schema.Respond("success", c)
}

// GetAllTasks
// WhoAmi godoc
// @Summary Получение всех задач
// @Accept json
// @Produce json
// @Success 200 {object} schema.Response[[]model.Task]
// @Failure 400 {object} schema.Response[schema.Empty]
// @tags tasks
// @Router /api/v1/tasks [get]
func (h *Handler) GetAllTasks(c *gin.Context) error {
	ctx := c.Request.Context()
	tasks, err := h.services.Task.GetAll(ctx)
	if err != nil {
		return err
	}
	return schema.Respond(tasks, c)
}
