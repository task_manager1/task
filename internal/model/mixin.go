package model

import (
	"time"
)

type DeleteMixin struct {
	DeletedAt *time.Time `json:"deleted_at" gorm:"column:deleted_at"`
}

type TimestampMixin struct {
	CreatedAt time.Time `json:"created_at" gorm:"column:created_at"`
	UpdatedAt time.Time `json:"updated_at" gorm:"column:updated_at"`
}
