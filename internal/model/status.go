package model

type Status struct {
	TimestampMixin
	ID   uint64 `gorm:"column:id" json:"id"`
	Nane string `gorm:"column:name" json:"name"`
	Code uint64 `gorm:"column:code" json:"code"`
}

func (a Status) TableName() string {
	return "task.statuses"
}
