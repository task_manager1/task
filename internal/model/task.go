package model

type Task struct {
	TimestampMixin
	ID          uint64 `gorm:"column:id" json:"id"`
	Title       string `gorm:"column:title" json:"title"`
	Description string `gorm:"column:description" json:"description"`
	Creator     uint64 `gorm:"column:creator" json:"creator"`
	Executor    uint64 `gorm:"column:executor" json:"executor"`
	Status      uint64 `gorm:"column:status" json:"status"`
}

func (a Task) TableName() string {
	return "task.tasks"
}
