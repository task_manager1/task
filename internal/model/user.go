package model

type User struct {
	TimestampMixin
	ID         uint64 `gorm:"column:id" json:"id"`
	Username   string `gorm:"column:username" json:"username"`
	Password   string `gorm:"column:password" json:"password"`
	FirstName  string `gorm:"column:first_name" json:"first_name"`
	LastName   string `gorm:"column:last_name" json:"last_name"`
	MiddleName string `gorm:"column:middle_name" json:"middle_name"`
}

func (a User) TableName() string {
	return "task.users"
}
