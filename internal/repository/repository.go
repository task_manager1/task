package repository

import (
	"gitlab.com/task_manager1/proto/gorpc"
	"gitlab.com/task_manager1/task/internal/repository/database"
	"gitlab.com/task_manager1/task/internal/repository/grpc"
	"gorm.io/gorm"
)

type Repositories struct {
	TaskDB           database.Tasker
	NotificationGrpc grpc.Notificationer
}

func NewRepositories(db *gorm.DB, client gorpc.NotificationClient) *Repositories {
	taskDB := database.NewTaskDB(db)
	notificationGrpc := grpc.NewNotificationGrpc(client)

	return &Repositories{
		TaskDB:           taskDB,
		NotificationGrpc: notificationGrpc,
	}
}
