package grpc

import (
	"context"
	"gitlab.com/task_manager1/proto/gorpc"
	"gitlab.com/task_manager1/task/internal/model"
)

type NotificationGrpc struct {
	client gorpc.NotificationClient
}

func NewNotificationGrpc(client gorpc.NotificationClient) Notificationer {
	return &NotificationGrpc{
		client: client,
	}
}

func (r *NotificationGrpc) CreateNotification(
	ctx context.Context,
	task model.Task, userId uint64) (bool, error) {
	_, err := r.client.CreateNotification(ctx, &gorpc.CreateNotificationReqDataV1{
		Title:  task.Title,
		Text:   task.Description,
		TaskId: task.ID,
		UserId: userId,
	})
	if err != nil {
		return false, err
	}

	return true, nil
}
