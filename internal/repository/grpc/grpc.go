package grpc

import (
	"context"
	"gitlab.com/task_manager1/task/internal/model"
)

type Notificationer interface {
	CreateNotification(ctx context.Context, task model.Task, userId uint64) (bool, error)
}
