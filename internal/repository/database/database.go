package database

import (
	"context"
	"gitlab.com/task_manager1/task/internal/model"
)

type Tasker interface {
	Create(ctx context.Context, task model.Task) (err error)
	Delete(ctx context.Context, id uint64) (err error)
	Get(ctx context.Context, id uint64) (task model.Task, err error)
	Update(ctx context.Context, task model.Task) (err error)
	GetAll(ctx context.Context) (tasks []model.Task, err error)
}
