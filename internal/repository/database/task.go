package database

import (
	"context"
	"gitlab.com/task_manager1/task/internal/model"
	"gorm.io/gorm"
)

type TaskDB struct {
	db *gorm.DB
}

func NewTaskDB(
	db *gorm.DB,
) *TaskDB {
	return &TaskDB{
		db: db,
	}
}

func (r *TaskDB) Create(ctx context.Context, task model.Task) (err error) {
	err = r.db.WithContext(ctx).Model(&model.Task{}).Create(&task).Error
	if err != nil {
		return err
	}

	return nil
}

func (r *TaskDB) Delete(ctx context.Context, id uint64) (err error) {
	err = r.db.WithContext(ctx).Model(&model.Task{}).Delete(model.User{}, id).Error
	if err != nil {
		return err
	}

	return nil
}

func (r *TaskDB) Get(ctx context.Context, id uint64) (task model.Task, err error) {
	err = r.db.WithContext(ctx).Model(&model.Task{}).Where("id = ?", id).First(&task).Error
	if err != nil {
		return model.Task{}, err
	}
	return task, nil
}

func (r *TaskDB) Update(ctx context.Context, task model.Task) (err error) {
	err = r.db.WithContext(ctx).Model(&model.Task{}).Where("id = ?", task.ID).
		Updates(model.Task{
			Title:       task.Title,
			Description: task.Description,
			Status:      task.Status}).Error
	if err != nil {
		return err
	}
	return nil
}

func (r *TaskDB) GetAll(ctx context.Context) (tasks []model.Task, err error) {
	err = r.db.WithContext(ctx).Model(&model.Task{}).Find(&tasks).Error
	if err != nil {
		return []model.Task{}, err
	}
	return tasks, nil
}
