package middleware

import (
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io"
	"io/ioutil"
)

type MiddlewareConfig struct {
	logRequestBody  bool
	logResponseBody bool
}

type MiddlewareOption func(config *MiddlewareConfig)

// WithLogRequestBody определяет будет ли логироваться тело запроса.
// По-умолчанию тело запроса логгируется
func WithLogRequestBody(logRequestBody bool) MiddlewareOption {
	return func(config *MiddlewareConfig) {
		config.logRequestBody = logRequestBody
	}
}

// WithLogResponseBody определяет будет ли логироваться тело ответа.
// По-умолчанию тело ответа логгируется
func WithLogResponseBody(logResponseBody bool) MiddlewareOption {
	return func(config *MiddlewareConfig) {
		config.logResponseBody = logResponseBody
	}
}

type bodyLogWriter struct {
	gin.ResponseWriter
	body *bytes.Buffer
}

func (w bodyLogWriter) Write(b []byte) (int, error) {
	w.body.Write(b)
	return w.ResponseWriter.Write(b)
}

func (w bodyLogWriter) WriteString(s string) (int, error) {
	w.body.WriteString(s)
	return w.ResponseWriter.WriteString(s)
}

func New(opts ...MiddlewareOption) gin.HandlerFunc {
	cfg := &MiddlewareConfig{
		logRequestBody:  true,
		logResponseBody: true,
	}

	for _, opt := range opts {
		opt(cfg)
	}

	return func(c *gin.Context) {
		var (
			responseWriter *bodyLogWriter
		)

		if cfg.logResponseBody {
			responseWriter = &bodyLogWriter{
				ResponseWriter: c.Writer,
				body:           bytes.NewBuffer(make([]byte, 0, 2048)),
			}
			c.Writer = responseWriter
		}

		c.Next()
	}
}

func getRequestBody(c *gin.Context) (map[string]interface{}, bool) {
	var buf bytes.Buffer
	tee := io.TeeReader(c.Request.Body, &buf)
	body, err := ioutil.ReadAll(tee)
	if err != nil {
		return nil, false
	}

	c.Request.Body = ioutil.NopCloser(&buf)

	return parseBody(body)
}

func getResponseBody(buf *bytes.Buffer) (map[string]interface{}, bool) {
	body, err := ioutil.ReadAll(buf)
	if err != nil {
		return nil, false
	}

	return parseBody(body)
}

func parseBody(rawBody []byte) (map[string]interface{}, bool) {
	var body map[string]interface{}

	if err := json.Unmarshal(rawBody, &body); err != nil {
		return nil, false
	}

	return body, true
}
