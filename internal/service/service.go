package service

import (
	"context"
	"gitlab.com/task_manager1/task/internal/config"
	"gitlab.com/task_manager1/task/internal/model"
	"gitlab.com/task_manager1/task/internal/repository"
)

type Task interface {
	Create(ctx context.Context, task model.Task, userId uint64) (err error)
	Get(ctx context.Context, id uint64) (task model.Task, err error)
	Update(ctx context.Context, task model.Task) (err error)
	Delete(ctx context.Context, id uint64) (err error)
	GetAll(ctx context.Context) (tasks []model.Task, err error)
}

type Services struct {
	Task Task
}

type Deps struct {
	Repos *repository.Repositories
	Cgf   *config.Config
}

func NewServices(deps Deps) *Services {
	taskService := NewTaskService(deps.Repos)

	return &Services{
		Task: taskService,
	}
}
