package service

import (
	"context"
	"fmt"

	"gitlab.com/task_manager1/task/internal/model"
	"gitlab.com/task_manager1/task/internal/repository"
)

type TaskService struct {
	repos *repository.Repositories
}

func NewTaskService(
	repos *repository.Repositories,
) *TaskService {
	return &TaskService{
		repos: repos,
	}
}

func (s *TaskService) Create(ctx context.Context, task model.Task, userId uint64) (err error) {
	err = s.repos.TaskDB.Create(ctx, task)
	if err != nil {
		return err
	}
	result, err := s.repos.NotificationGrpc.CreateNotification(ctx, task, userId)
	if err != nil {
		return err
	}
	fmt.Print(result)
	return nil
}

func (s *TaskService) Get(ctx context.Context, id uint64) (task model.Task, err error) {
	task, err = s.repos.TaskDB.Get(ctx, id)
	if err != nil {
		return model.Task{}, err
	}
	return task, nil
}

func (s *TaskService) Update(ctx context.Context, task model.Task) (err error) {
	err = s.repos.TaskDB.Update(ctx, task)
	if err != nil {
		return err
	}
	return nil
}

func (s *TaskService) Delete(ctx context.Context, id uint64) (err error) {
	err = s.repos.TaskDB.Delete(ctx, id)
	if err != nil {
		return err
	}
	return nil
}

func (s *TaskService) GetAll(ctx context.Context) (tasks []model.Task, err error) {
	tasks, err = s.repos.TaskDB.GetAll(ctx)
	if err != nil {
		return []model.Task{}, err
	}
	return tasks, nil
}
