package postgresql

import (
	"database/sql"
	"fmt"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/plugin/dbresolver"
)

func NewDB(sources []string, replicas []string) (*sql.DB, *gorm.DB, error) {
	fmt.Print(
		"Start connecting to DB",
		"sources", sources,
		"replicas", replicas,
	)

	if len(sources) == 0 {
		return nil, nil, fmt.Errorf("sources database connetions is empty")
	}

	db, err := gorm.Open(
		postgres.New(postgres.Config{
			DSN:                  sources[0],
			PreferSimpleProtocol: true, // disables implicit prepared statement usage
		}),
	)
	if err != nil {
		return nil, nil, err
	}

	if len(sources) > 1 || len(replicas) > 0 {
		fmt.Print("Database: setup multi source/replica")

		sources = sources[1:]
		sourceConnections := make([]gorm.Dialector, len(sources))
		replicaConnections := make([]gorm.Dialector, len(replicas))

		for i, sourceDSN := range sources {
			sourceConnections[i] = postgres.Open(sourceDSN)
		}

		for i, replicaDSN := range replicas {
			replicaConnections[i] = postgres.Open(replicaDSN)
		}

		err = db.Use(dbresolver.Register(
			dbresolver.Config{
				Sources:  sourceConnections,
				Replicas: replicaConnections,
				Policy:   dbresolver.RandomPolicy{},
			},
		))
		if err != nil {
			return nil, nil, err
		}
	}

	connection, err := db.DB()
	if err != nil {
		return nil, nil, err
	}

	connection.SetConnMaxLifetime(time.Hour)
	connection.SetMaxIdleConns(15)
	connection.SetMaxOpenConns(15)

	fmt.Print("DB connected")

	return connection, db, nil
}
